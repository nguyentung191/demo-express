const express = require('express');
const bodyParser= require('body-parser')
const MongoClient = require('mongodb').MongoClient;
const session = require('express-session');
const app = express();


app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());
app.use(express.static('public'));
app.use(session({secret: 'mySecret', resave: false, saveUninitialized: false}));


app.set('view engine','ejs');

const connectionString = 'mongodb+srv://nthanhtung9x:tung1998@cluster0-zsy3m.mongodb.net/<dbname>?retryWrites=true&w=majority'
MongoClient.connect(connectionString, { useUnifiedTopology: true })
.then(client => {
    console.log('Connected to Database');
    const db = client.db('demo');
    const userCollection = db.collection('users');
    let listUser = [];
    db.collection('users').find().toArray()
    .then(result => listUser = [...result])
    .catch(err => console.log(err))

    app.get('/',(req,res) => {
        req.session.errorMessage = null;
        res.render('index',{user: req.session.user || null});
    })
   
    // sign up
    app.get('/signup', (req,res) => {
        db.collection('users').find().toArray()
        .then(result => 
            res.render('signup',{
                errorMessage: req.session.errorMessage || null,
                user: null,
                listUser : [...result]
            }),
            console.log(listUser)
        )
        .catch(err => console.log(err))
        
    })
    app.post('/signup', (req,res) => {
        let email = req.body.email;
        let findUser = listUser.findIndex((item) => {
            return item.email === email;
        })
        if(findUser !== -1){
            req.session.errorMessage = "Tài khoản đã tồn tại";
            res.render('signup', {
                errorMessage: req.session.errorMessage || null,
                user: null
            });
        }
        else {
            userCollection.insertOne(req.body)
            .then(result => {
                res.redirect('/signin');
            })
            .catch(error => console.error(error))  
        }
    })

    // sign in
    app.get('/signin', (req,res) => {
        res.render('signin',{errorMessage:null,user:null})
    })
    app.post('/signin',(req,res) => {
        let email = req.body.email;
        let password = req.body.password;
        let findUser = listUser.findIndex((item) => {
            return item.email === email && item.password === password;
        })

        if(findUser === -1){
            console.log('failed');
            res.render('signin',{
                errorMessage: "Tài khoản hoặc mật khẩu không đúng",
                user:null
            })
        }
        else {
            req.session.errorMessage = null;
            req.session.user = listUser[findUser];
            res.redirect('/');
        }
    })

    // log out
    app.get('/logout', (req,res) => {
        req.session.user = null;
        res.render('index',{user:null});
    })

    // manager
    app.get('/manager', (req,res) => {
        if(req.session.user) {
            db.collection('users').find().toArray()
            .then(result => 
                res.render('manager',{
                    user: req.session.user || null,
                    listUser : [...result]
                })
            )
            .catch(err => console.log(err))
        } else {
            res.redirect('/');
        }
    })

    // delete user
    app.delete('/manager', (req,res) => {
        userCollection.deleteMany({
            email: req.body.email
        })
        .then(result => {
            if(result.deletedCount === 0) {
                return res.json('failed');
            } 
            res.json('success');
        })
        .catch(err => console.err(err));
    })

    // update user
    app.put('/manager', (req,res) => {
        userCollection.findOneAndUpdate(
            { 
                email: req.body.email
            },
            {
                $set: {
                    password: req.body.password
                }
            }, 
            {
                upsert: false
            }
        )
        .then(result => {
            if(!result.value) {
                return res.json('failed');
            } 
            res.json('success')
        })
        .catch(err => console.error(err))
    })

})
.catch(error => console.error(error))


app.listen(3000, () => console.log('Listening on 3000'));