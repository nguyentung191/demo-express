
const deleteButton = document.getElementById('delete-btn');
const deleteMessage = document.getElementById('deleteMessage');
deleteButton.addEventListener("click", () => {
    const deleteValue = document.getElementById('deleteEmail').value;
    fetch('/manager', {
        method:'delete',
        headers: {'Content-Type' : 'application/json'},
        body: JSON.stringify({
            email: deleteValue
        })
    })
    .then(res => {
        if(res.ok) return res.json()
    })
    .then(response => {
        if(response === 'failed') {
            deleteMessage.innerHTML = "Xoá thất bại";
        } else {
            window.location.reload(true);
        }
    })
    .catch(err => console.error(err));
})

const updateButton = document.getElementById('update-btn');
const updateMessage = document.getElementById('updateMessage');
updateButton.addEventListener("click", () => {
    const updateEmail = document.getElementById('updateEmail').value;
    const updatePassword = document.getElementById('updatePassword').value;
    
    fetch('/manager', {
        method:'put',
        headers: {'Content-Type' : 'application/json'},
        body: JSON.stringify({
            email: updateEmail,
            password: updatePassword
        })
    })
    .then(res => {
        if(res.ok) return res.json()
    })
    .then(response => {
        if(response === 'failed') {
            updateMessage.innerHTML = "Cập nhật thất bại";
        } else {
            window.location.reload(true);
        }
    })
    .catch(err => console.error(err));
})
